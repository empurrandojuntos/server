# In this file are described all signals in the project
# This is used to manage signals features in the tests and to avoid
# conflicts between similar signals

# Signals are described with a unique string identifier and a description
# about its purpose

SIGNALS = {
    'compute_clusters_for': {
        'uid': '0001',
        'app': 'conversations',
        'class': 'Vote',
        'type': 'post_save',
        'description': 'Compute clusters for a conversation after saving a new vote.',
        'source': 'clusters/signals.py',
    },

    'increase_participation_from_comment': {
        'uid': '0002',
        'app': 'conversations',
        'class': 'Comment',
        'type': 'post_save',
        'description': 'Compute clusters for a conversation after saving a new vote.',
        'description': 'Increase number of participants in a conversation after new comment.',
        'source': 'conversations/signals.py',
    },

    'increase_participation_from_vote': {
        'uid': '0003',
        'app': 'conversations',
        'class': 'Vote',
        'type': 'pre_save',
        'description': 'Compute clusters for a conversation after saving a new vote.',
        'description': 'Increase number of participants in a conversation after new vote.',
        'source': 'conversations/signals.py',
    },
}
