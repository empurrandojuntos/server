from clusters.models import *
from unittest import TestCase
from model_mommy import mommy
from unittest.mock import MagicMock
from numpy.testing import assert_array_equal
from numpy.testing import assert_array_almost_equal
import numpy as np

from django.db.utils import IntegrityError

from clusters.test.test_helper import *


@pytest.fixture()
def setup():
    conversation = mommy.make('conversations.Conversation')
    return (conversation)


@pytest.mark.django_db
def test_should_be_able_to_save_cluster_data_with_required_fields(setup):
    conversation = setup
    cluster_data = ClusterData(conversation=conversation)

    cluster_data.save()
    cluster_data.refresh_from_db()


@pytest.mark.django_db
def test_should_not_save_cluster_data_without_required_fields():
    cluster_data = ClusterData()
    with pytest.raises(Exception) as err:
        cluster_data.save()

    assert err.type == IntegrityError
    assert not cluster_data.conversation_id


@pytest.mark.django_db
def test_cluster_data_should_be_able_to_save_numpy_array_of_labels(setup):
    conversation = setup
    labels = np.array([0, 0, 0, 1, 1, 0, 1, 0])
    cluster_data = ClusterData(conversation=conversation, labels=labels)

    cluster_data.save()
    cluster_data.refresh_from_db()

    assert_array_equal(labels, cluster_data.labels)


@pytest.mark.django_db
def test_cluster_data_should_be_able_to_save_numpy_array_of_members(setup):
    conversation = setup
    members = np.array([[1, 1], [0, 1], [1, 0], [1, 0]])
    cluster_data = ClusterData(conversation=conversation, members=members)

    cluster_data.save()
    cluster_data.refresh_from_db()

    assert_array_equal(members, cluster_data.members)


@pytest.mark.django_db
def test_cluster_data_should_be_able_to_save_numpy_array_of_centroids(setup):
    conversation = setup
    centroids = np.array([[1, 1], [0, 1], [1, 0], [1, 0]])
    cluster_data = ClusterData(conversation=conversation, centroids=centroids)

    cluster_data.save()
    cluster_data.refresh_from_db()

    assert_array_equal(centroids, cluster_data.centroids)
