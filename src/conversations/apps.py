from django.apps import AppConfig

from django.db.models.signals import pre_save, post_save
from django.conf import settings

class ConversationsConfig(AppConfig):
    name = 'conversations'

    def ready(self):
        from conversations.signals import increase_participation_from_vote
        from conversations.signals import increase_participation_from_comment

        pre_save.connect(
            increase_participation_from_vote,
            sender='conversations.Vote',
            dispatch_uid=settings.SIGNALS["increase_participation_from_vote"]["uid"],
        )

        post_save.connect(
            increase_participation_from_comment,
            sender='conversations.Comment',
            dispatch_uid=settings.SIGNALS["increase_participation_from_comment"]["uid"],
        )
