from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.ConversationView.as_view(), name="conversations"),
    url(r'^comments/$', views.CommentView.as_view(), name="comments"),
    url(r'^votes/$', views.VoteView.as_view(), name="votes"),
]
